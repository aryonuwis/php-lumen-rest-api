<?php

/*
|--------------------------------------------------------------------------
| api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post(
    'auth/login',
    [
        'uses' => 'AuthController@authenticate'
    ]
);

// Users Office/Administrator
$router->group(['middleware' => 'jwt.authoffice'],

    function () use ($router) {

        // Account Control
        $router->get('users/list', 'UsersController@account_list');
        $router->get('users/view', 'UsersController@account_views');

        $router->post('users/register', 'UsersController@account_regiter');
        $router->post('users/update', 'UsersController@account_update');
        $router->post('users/photo/upload', 'UsersController@photo_profile');

        // Navigatioan Control
        $router->get('nav/all_menu', 'NavController@all_list_navigation');
        $router->get('nav/get_used', 'NavController@used_navigation_users');
        $router->get('nav/get_detail', 'NavController@view_detail_navigation');

        $router->post('nav/add_nav/{type}', 'NavController@add_navigation');// {type} meupakan jenis menu apabila di menu utaman maka type berisi 'main' apa bila dia cabang value 'sub'
        $router->post('nav/edit_nav', 'NavController@edit_navigation');
        $router->post('nav/delete_nav', 'NavController@delete_navigation');

        $router->get('credintial/get_list', 'CredintialController@getlist_credintial');

        $router->post('credintial/set_navigation','CredintialController@credintialset');

    }

);
