<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\DB;

class Users_account extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //tabel name
    public $timestamps = false;
    public $table = "users_account";
    protected $fillable = [
        'name', 'email', 
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','id_credential','email','phone'
    ];

    public function __construct()
    {
        $this->tabels = "users_account";
    }

    public function account_updates($users='',$user_id)
    {
       
        return DB::table($this->tabels)
                ->where('id',$user_id)
                ->update([
                    'first_name'    => $users['first_name'],
                    'last_name'     => $users['last_name'],
                    'usersname'     => $users['usersname'],
                    'email'         => $users['email'],
                    'phone'         => $users['phone'],
                    'id_credential' => $users['users_type'],
                    'update_by'     => $user_id,
                    'update_at'     => date('Y-m-d H:i:s')
                ]);
        
    }

    public function account_list()
    {
        return DB::table($this->tabels)
                ->select(
                    'id', 
                    'first_name', 
                    'usersname', 
                    'email'
                    )
                    ->get()
                    ->toArray();
    }

    public function account_view($users_id)
    {
        return DB::table($this->tabels)
            ->select(
                'id',
                'first_name',
                'last_name',
                'usersname',
                'email',
                'email_status',
                'phone',
                'phone_status',
                'id_credential'
            )
            ->where('id', $users_id)
            ->first();
        }
    
    public function save_image_profile($user_id,$imge_name)
    {
        return DB::table($this->tabels)
                    ->where('id',$user_id)
                    ->update([
                        'photo_profile' => $imge_name,
                        'update_by'     => $user_id,
                        'update_at'     => date('Y-m-d H:i:s')
                    ]);   
    }
    
}
