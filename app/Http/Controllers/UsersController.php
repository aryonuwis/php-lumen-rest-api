<?php

namespace App\Http\Controllers;

use App\Users_account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->dbs          = new Users_account();
        $this->user_id      = $request->auth->id;
        $this->user_email   = $request->auth->email;
        $this->header       =  array(
                                      'Content-Type'=> 'application/json',
                                      'version' => '1.0'
                                   );
    }

    // List Users
    public function account_list(Request $request){
      
      $result = $this->dbs->account_list();

      foreach ($result as $key => $value) {
          $result[$key]->id_enc = $this->encripytion($value->id);
          unset($result[$key]->id);
      }

      if(!empty($result)){
        return response()->json([
                          'status' => 'success',
                          'result' => $result
                          ],200,$this->header
                      );
      }else{
        return response()->json(array(
                                'status' =>'filed',
                                'result' => 'data tidak di temukan'
                                ), 400, $this->header);  
      }
    }

    // view users data
    public function account_views(Request $request){
        
        $result = $this->dbs->account_view($this->decryption($request->input('id_users'))); 
        
        if (!empty($result)) {

            $result->id_enc = $this->encripytion($result->id);
            unset($result->id);
            return response()->json([
                                          'status' => 'success',
                                          'result' => $result
                                    ], 200, $this->header);
        }else {

            return response()->json([
                                          'status' => 'failed',
                                          'result' => 'users tidak di temukan'
            ], 400, $this->header);
        
                                      }
    }

    // untuk authentification login 
    public function account_regiter(Request $request){

      $res_validate = Validator::make($request->all(),[
                                        "first_name"=> "required",
                                        "usersname" => "required|unique:users_account",
                                        "email"     => "required|email|unique:users_account",
                                        "phone"     => "required|numeric",
                                        "password"  => "required|min:6|regex:/^[a-zA-Z0-9!$#%]+$/",
                                        "users_type"=> "required"
                                        ]);

      if ($res_validate->fails()) {
        return response()->json([
          'status' =>'failed',
          'result'=> $res_validate->errors()->all()
        ], 401, $this->header);
      }

    $this->dbs->first_name    = $request->input('first_name');
    $this->dbs->last_name     = $request->input('last_name');
    $this->dbs->usersname     = $request->input('usersname');
    $this->dbs->email         = $request->input('email');
    $this->dbs->phone         = $request->input('phone');
    $this->dbs->password      = Hash::make(md5($request->input('password')));
    $this->dbs->id_credential = $request->input('users_type');
    $this->dbs->create_by     = $this->user_id;
    $this->dbs->create_at     = date('Y-m-d H:i:s');
    $this->dbs->status        = "1"; 
    $result = $this->dbs->save();

    if ($result) {
      return response()->json(array(
          'status' => 'success' ,
          'result' => 'Users '. $request->input('first_name').' berahasil di tambahakan' 
        ), 200, $this->header);
    }else {
      return response()->json(array(
        'status' => 'failed',
        'result' => 'Users' . $request->input('first_name') . ' berahasil di tambahakan'
      ), 401, $this->header);
    }
  
  }

  // unutk Update users 
  public function account_update(Request $request){

      if ($this->decryption($request->input('id_users'))!="FAILED") {
            
          $validate_res = Validator::make($request->all(), [
                "id_users"    =>  "required",
                "first_name"  =>  "required",
                "usersname"   =>  "required|unique:users_account,usersname," .$this->decryption($request->input('id_users')),
                "email"       =>  "required|email|unique:users_account,email," .$this->decryption($request->input('id_users')),
                "phone"       =>  "required|numeric",
                "users_type"  =>  "required",
              ]);

            if ($validate_res->fails()) {
                return response()->json([
                  'status'=> "failed",
                  'result'=> $validate_res->errors()->all()
                ], 400, $this->header);
            }

            $result = $this->dbs->account_updates($request->all(), $this->decryption($request->input('id_users'))); 

            if ($result) {
              return response()->json([
                'status' => "success",
                'result' => "Data berhasil di perbaharui"
              ], 200, $this->header); 
            }else {
        return response()->json([
                  'status' => "failed",
                  'result' => "Silahkan coba beberapa saat lagi"
        ], 200, $this->header);
            }
            
      }else{
        return response()->json(array(
          'status' => "failed",
          'result' => "failed users id"
        ),400, $this->header);
      }

  }

  public function photo_profile(Request $request){
    
    $file           = $request->file('photo_profiles');
  //  $file_name      = $file->getClientOriginalName();
    $file_name_set  = date('YmdHis').'.png'; 

    $ori_upload_images = array(
                          'files'=>$file,
                          'file_name'=> $file_name_set,
                          'path_ori'=> 'upload/image/ori/profile/', // path original image
                        );
  
    $other_size_img = array(
                        'set_image' => 'fit',
                        'set_size'  =>'200|500',//jika ukuran yang akan di set lebih dari 1 diberi pembatas '|' gambar selalu squer
                        'path_othersize'=> 'upload/image/thumb200/profile/|upload/image/thumb500/profile/',//jika ukuran yang akan di set lebih dari 1 diberi pembatas '|'
                        'quality' => '60', // menentukan kualitas gambar 0-100 optioanl default 90
                      );
    
    $result_image = $this->upload_sets_images($ori_upload_images, $other_size_img);
    
    if ($result_image['status'] == 'success') {
        
        $ret_bd = $this->dbs->save_image_profile($this->user_id,$file_name_set);

        if ($ret_bd) {
            return response()->json(array(
                                      'status'  => 'success',
                                      'result'  => 'Foto profile berhasil di ubah'
                                    ), 200, $this->header);
        }else{
        return response()->json(array(
                                    'status'  => 'failed',
                                    'result'  => 'image gagal untuk di simpan silahkan hubungi developer'
                                  ), 400, $this->header);
        }

    }else {
      return response()->json(array(
                                'status'  => 'failed',
                                'result'  => $result_image['msg']  
                              ), 400, $this->header);
    }
  
  }



}
