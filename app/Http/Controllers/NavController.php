<?php

namespace App\Http\Controllers;

use App\Navigation;
use App\Users_account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class NavController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->dbs          = new Navigation();
        $this->dbus         = new Users_account();
        $this->user_id      = $request->auth->id;
        $this->user_email   = $request->auth->email;
        $this->header       =  array(
            'Content-Type' => 'application/json',
            'version' => '1.0'
        );
    }

    public function used_navigation_users(Request $request)
    {
        //   echo 'navigasi yang tampil pada halaman dasboard user office';
        $credintial_id  = $this->dbus->account_view($this->user_id)->id_credential;
        $result         = $this->dbs->get_nav_used($credintial_id);

        foreach ($result as $key => $value) {
            $result[$key]->sub_menu = $this->dbs->get_all_chiled($value->id_navgation);
            if (count($result[$key]->sub_menu)>0) {
                foreach ($result[$key]->sub_menu as $key2 => $value2) {
                    $result[$key]->sub_menu[$key2]->sub_menu = $this->dbs->get_all_chiled($value2->id_navgation);
                    unset($result[$key]->sub_menu[$key2]->id_navgation);
                }
            }
            unset($result[$key]->id_navgation);
        }
        
        if (count($result)>0) { 
            return response()->json(array(
                    'status'    =>'success',
                    'result'    => $result
            ), 200,$this->header);

        }else {
            return response()->json(array(
                'status' => 'filed',
                'result' => 'data tidak di temukan'
            ), 400, $this->header);
        
        }
    }

    public function all_list_navigation(Request $request)
    {
        //echo 'for all list menu office';
        $result = $this->dbs->get_all_parent();
        foreach ($result as $key => $value) {
            $result[$key]->id_navenc    = $this->encripytion($value->id_navgation);
            $result[$key]->sub_menu     = array();
            $result[$key]->sub_menu     = $this->dbs->get_all_chiled($value->id_navgation);
            if (count($result[$key]->sub_menu)!='') {
   
                foreach ($result[$key]->sub_menu as $key2 => $value2) {
                    $result[$key]->sub_menu[$key2]->id_navenc   = $this->encripytion($value2->id_navgation);
                    $result[$key]->sub_menu[$key2]->sub_menu    =  $this->dbs->get_all_chiled($value2->id_navgation);
                }
            }
        }

        return response()->json($result, 200, $this->header);
    }

    public function add_navigation(Request $request,$type)
    {
        if ($type=='main') {
            $input_val = Validator::make($request->all(),[
                'name'=> 'required|regex:/^[\pL\s\-]+$/u',
                'slug'=>'required',
                'order_list'=>'required'
            ]);
        }else {
            $input_val = Validator::make($request->all(),[
                'name'=> 'required|regex:/^[\pL\s\-]+$/u',
                'slug' => 'required',
                'order_list' => 'required',
                'parent_id' => 'required'
            ]);        
        }

        if ($input_val->fails()) {
            return response()->json([
                'status'=>'failed',
                'result'=> $input_val->errors()->all()
            ], 401, $this->header);
        }

        $retrn_db = $this->dbs->insert_nav($request->all(),$type, $this->user_id);

        if ($retrn_db) {
            return response()->json([
                'status'=> 'success',
                'result'=> 'Data Berhasil di simpan'
            ], 200, $this->header);
        }else {
            return response()->json([
                'status'=>'failed',
                'result'=>'Data gagal di simpan silahkan coba beberapa saat lagi'
            ], 401, $this->header);
        }
    }

    public function view_detail_navigation(Request $request)
    {
        // echo 'for all detail menu office';
        if ( !empty($request->input('id')) && $this->decryption($request->input('id'))!='FAILED') {
            $result = $this->dbs->get_detail_navigation($this->decryption($request->input('id')));
            $result->id_enc = $this->encripytion($result->id_enc);
            return response()->json([
                'status' => 'success',
                'result' =>  $result 
            ], 200, $this->header);
        }else {
            return response()->json([
                'status' => 'failed',
                'result' => 'Id tidak di temukan atau salah'
            ], 401, $this->header);
        }
    }

    public function edit_navigation(Request $request){
        // echo 'for all edit menu office';
       if ($this->decryption($request->input('id_enc'))!='FAILED') {
            $req = $request->input();
            if (empty($request->input('nav_parent_id'))) {
                $req['nav_parent_id'] = '0';
            }
            $id_nav                     = $this->decryption($req['id_enc']);
            $data_edit['name']          = $req['nav_name'];
            $data_edit['icon']          = $req['nav_icon'];
            $data_edit['slug']          = $req['nav_slug'];
            $data_edit['order_list']    = $req['nav_order_list'];
            $data_edit['nav_parent_id'] = $req['nav_parent_id'];
            $data_edit['update_by']     = $this->user_id;
            $data_edit['update_date']   = date('Y-m-d H:i:s');

            $result = $this->dbs->edit_navigation_data($id_nav,$data_edit);
            if ($result) {
                return response()->json([
                    'status'=> 'success',
                    'result'=> 'navigation berhasil di ubah'
                ], 200, $this->header);
            }else{
                return response()->json([
                    'status'=>'failed',
                    'result'=>'system errors'
                ], 401, $this->header);
            }
       }else {
        return response()->json([
            'status'=>'failed',
            'result'=> 'id navigation tidak tepat'
        ], 401, $this->header);
       }
    }

    public function delete_navigation(Request $request){
        // echo 'for all delete menu office';
        if ($this->decryption($request->input('id_enc'))!='FAILED') {
            $result = $this->dbs->delete_navigation($this->decryption($request->input('id_enc')),$this->user_id);
            if ($result) {
                return response()->json([
                    'status'=>'success',
                    'result'=> 'Data berhasil di hapus'
                ], 200, $this->header);
            }else {
                return response()->json([
                    'status'=>'failed',
                    'result'=>'system error'
                ], 200, $this->header);
            }
        }else{
            return response()->json([
                'status'=>'failed',
                'result'=>'id navigation invalid'
            ], 401, $this->header);
        }
    }

    
}
