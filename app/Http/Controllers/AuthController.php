<?php

namespace App\Http\Controllers;

use Validator;
use App\Users_account;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends Controller
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * Authenticate a Users_account and return the token if the provided credentials are correct.
     * 
     * @param  \App\Users_account   $Users_account 
     * @return mixed
     */

    public function authenticate(Users_account $Users_account)
    {

        $this->validate($this->request, [
            'email'     => 'required',
            'password'  => 'required'
        ]);
   
        // Find the Users_account by email
        $Users_account = Users_account::where('email', $this->request->input('email'))->first();

        if (!$Users_account) {
            return response()->json([
                "status"=> 'FAILED',
                'msg' => 'Email does not exist.'
            ], 400);
        }

        // Verify the password and generate the token
        if ($this->decryption(Hash::check(md5($this->request->input('password')), $Users_account->password))) {
            $get_token = $this->jwt($this->encripytion($Users_account->id));
            if ($get_token) {
                
                return response()->json([
                    'status' => 'SUCCESS',
                    'token' => $get_token
                ], 200);    
            
            }else{
                
                return response()->json([
                    'status' => 'FAILED',
                    'token' => NULL
                ], 400);
            
            }
            
        }

        // Bad Request response
        return response()->json([
            "status" => 'FAILED',
            'error' => 'Email or password is wrong.'
        ], 400);
    }

}