<?php

namespace App\Http\Controllers;

use App\Credintial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class CredintialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
     public function __construct(Request $request )
    {
        $this->dbs          = new Credintial();
        $this->user_id      = $request->auth->id;
        $this->user_email   = $request->auth->email;
        $this->header       =  array(
            'Content-Type' => 'application/json',
            'version' => '1.0'
        );
    }

    public function getlist_credintial(){
        
        

    }

    public function credintialset(Request $req){
        $data = json_decode($req->getContent());
        if (!empty($data)) {

            $credintial['id_credential']    =  $data->credintial_id;
            $credintial['name']             =  $data->credintial_name;
            $credintial['description']      =  $data->desc;
            $credintial['users']            =  $this->user_id;
        
            
            if ($data->credintial_id==NULL) { 
               // create credintial
                unset($credintial['id_credential']);
                $res_credintial = $this->dbs->add_credintial($credintial); 
                
                if (is_numeric($res_credintial)) {
                    
                    if (isset($data->credintial_set)) {
                       foreach ($data->credintial_set as $key => $value) {
                            $result_set_nav[] = $this->dbs->add_setcredintial($this->user_id, $res_credintial ,$value);
                        }
                        if (implode(' AND ',$result_set_nav)) {
                            return response()->json([
                                'status'=>'success',
                                'result'=> 'Data berhasi unutuk di update '
                            ], 200, $this->header);
                        }else {
                            return response()->json([
                                'status'=>'failde',
                                'result'=>'terjadi kesalahan pada pencocokan data'
                            ], 401, $this->header);
                        }
                    }else{
                        return response()->json([
                            'status'=>'failed',
                            'result'=>'navigation tidak ditemukan'
                        ], 401, $this->header);
                    }
                }else {
                    return response()->json([
                        'status'=>'failed',
                        'result'=>'terjadi kesalahan pada input credintial'
                    ], 401, $this->header);
                }
                
            } else if( $this->decryption($data->credintial_id) != 'FAILED') {
                


            }else{
                return response()->json([
                   'status'=>'failed',
                   'result'=>'Data tidak sesuai'
               ], 401, $$this->header);
            }
        }else {
            return response()->json([
                'status'=>'failed',
                'result'=>'Tidak ada data yang di input atau format data salah'
            ], 401, $this->header);
        }
    }



}
