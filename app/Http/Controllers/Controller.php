<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Firebase\JWT\JWT;
use Intervention\Image\ImageManagerStatic as Images;
// use Images;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    public function __construct(Request $request)
    {
        $this->header       =  array(
            'Content-Type' => 'application/json',
            'version' => '1.0'
        );

    }

    public function cekmethode(Request $request)
    {

        if (!in_array($request->method(), ['POST', 'GET'])) {
            return response()->json([
                    'error' => 'methode not use'
                ],400);
        }

    }

    // Endoce jwt
    protected function jwt($token_data)
    {
        $payload = [
            'iss' => "StockDulu_v1",      // Issuer of the token
            'sub' => $token_data,           // Subject of the token
            'iat' => time(),                // Time when JWT was issued. 
            'exp' => time() + 3600 * 3600       // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }


    public function encripytion($value)
    {
        return Crypt::encrypt($value);
    }

    public function decryption($value)
    {
        try {
            $decrypted = Crypt::decrypt($value);
        } catch (DecryptException $e) {
            $decrypted = "FAILED";
        }
        return $decrypted;
    }

    // Set size image
    public function upload_sets_images($ori=array(), $other=array())
    {
     if (count($ori)!=0) {
            if (count($other)!=0) {
                    $set_size   = explode('|', $other['set_size']);
                    $other_path = explode('|', $other['path_othersize']); 
                    if (!isset($other['quality'])|| $other['quality']=='') {
                        $quality = '90';
                    }else{
                        $quality = $other['quality'];
                    }
                    $funcset = $other['set_image'];
                if (count($set_size) == count($other_path)) {
                    for ($i=0; $i < count($set_size) ; $i++) {
                        $reimg_result[]  = Images::make($ori['files'])
                                                    ->$funcset($set_size[$i], $set_size[$i])
                                                    ->save($other_path[$i].$ori['file_name'], $quality)
                                                    ->response('png');
                    }
                    foreach ($reimg_result as $key => $value) {
                        if (!$value) {
                          
                            $other_size_error[] = $set_size[$key];
                        
                        }
                    }
                }else{
                    $return['status']  = 'failed';
                    $return['result']     = 'jumlah path dan ukuran tidak sama mohon periksa kembali';
                    return $return;
                }    
            }
            if (isset($other_size_error)) {   
                $return['status']  = 'failed';
                $return['result']     = 'image other size gagal di upload unutk ukuran'.implode('|', $other_size_error);
                return $return;
            }else{
                $original_img   = $ori['files']->move($ori['path_ori'], $ori['file_name']);
                if ($original_img) {
                    $return['status']  = 'success';
                    $return['result']     = 'image berhasil di upload';
                }else{
                    $return['status']  = 'failed';
                    $return['result']     = 'Original image gagal di upload ';
                }
                return $return;
            }
        }else{
            $return['status']  = 'failed';
            $return['result']     = 'Tidak ada gambar yang di unggah';
            return $return;
        }   
   
    }


    
}
