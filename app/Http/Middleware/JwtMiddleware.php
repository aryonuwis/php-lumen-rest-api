<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Users_account;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class JwtMiddleware
{
    const PARSED_METHODS = ['POST','GET','PATCH'];

    public function decryption($value)
    {
        try {
            $decrypted = Crypt::decrypt($value);
        } catch (DecryptException $e) {
            $decrypted = "FAILED";
        }
        return $decrypted;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        
        $token          = $request->header('Token');
        
        if (!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'Token not provided.'
            ], 401);
        }

        try {
        
            $credentials = JWT::decode($token, env('JWT_SECRET'),array('HS256'));
        
        } catch (ExpiredException $e) {
         
            return response()->json([
                'error' => 'Provided token is expired.'
            ], 400);
        
        } catch (Exception $e) {
        
            return response()->json([
                'error' => 'An error while decoding token.'
            ], 400);
        
        }

        if ($this->decryption($credentials->sub)== 'FAILED'|| $this->decryption($credentials->sub) == '') {
            return response()->json([
                'error' => 'you not users'
            ], 400);
        }

        $user = Users_account::find($this->decryption($credentials->sub));
        $request->auth = $user;
        
        return $next($request);
    }
}