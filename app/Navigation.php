<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\DB;
use App\Controller as mod_control;

class Navigation extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    public $timestamps = false;
    public $table   =   'navigation';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $fillable = [
        'id_navgation','name', 'icon', 'slug', 'order_list'
    ];

    protected $hidden = [
        'password',
    ];

    public function __construct()
    {
        $this->table = 'navigation';   
    }

    public function get_all_parent()
    {
        $result =  DB::table($this->table)
                            ->select('id_navgation',
                                    'name',
                                    'icon',
                                    'slug',
                                    'nav_parent_id')
                            ->where('status','1')
                            ->where('nav_parent_id','0')
                            ->reorder('order_list', 'asc')
                            ->get()
                            ->all();
        
        return  $result;     
    }
    
    public function get_all_chiled($id_parent=''){
        $result =array();
        if ($id_parent) {
            $result =  DB::table($this->table)
            ->select(
                'id_navgation',
                'name',
                'icon',
                'slug',
                'nav_parent_id'
            )
            ->where('status', '1')
            ->where('nav_parent_id', $id_parent)
            ->reorder('order_list', 'asc')
            ->get()
            ->all();
        }
        return $result;
    }

    public function get_nav_used($id_credintial='')
    {
        
        $result = DB::select("SELECT
                                navigation.id_navgation,
                                navigation.name,
                                navigation.slug,
                                navigation.order_list,
                                navigation.nav_parent_id as parent_id
                            FROM
                                navigation
                            INNER JOIN
                                set_credintial
                            ON
                                navigation.id_navgation=set_credintial.id_navgation
                            WHERE
                                navigation.status = '1'
                            AND
                                navigation.nav_parent_id = '0'
                            AND
                                set_credintial.id_credential= '".$id_credintial."'
                            ORDER BY
                                navigation.order_list ASC");
                    
        return $result;

    }


    public function insert_nav($data,$type,$user_id){
        
        if (!isset($data['parent_id'])|| $type=='main') {
            $parent_id = '0';    
        }else {
            $parent_id = $data['parent_id'];
        }

        if (!isset($data['icon'])) {
           $icon = NULL;
        }else {
            $icon = $data['icon'];
        }
        
        $result = DB::table($this->table)
                        ->insert([
                                    'name'          => $data['name'],
                                    'icon'          => $icon,
                                    'order_list'    => $data['order_list'],
                                    'slug'          => $data['slug'],
                                    'nav_parent_id' => $parent_id,
                                    'create_by'     => $user_id,
                                    'create_at'     => date('Y-m-d H:i:s'),
                                    'status'        => '1'
                                ]);
        return $result;
            
    }

    public function get_detail_navigation($data){
        $return = DB::table($this->table)
                    ->select(
                        'id_navgation as id_enc',
                        'name',
                        'icon',
                        'slug',
                        'order_list',
                        'nav_parent_id as parent_id'
                    )
                    ->where('id_navgation','=',$data)
                    ->where('status','=','1')
                    ->get()->first();
    
        return $return;
    }

    public function edit_navigation_data($id='',$data=array()){
        if ($id!=''&& !empty($data)) {
            $result = DB::table($this->table)
                ->where('id_navgation', $id)
                ->where('status', '!=', '0')
                ->update($data);
        }else {
            $result = false;
        }
        return $result;
    }


    public function delete_navigation($id='',$user_id=''){
        
        if ($id!='' && $user_id!='') {
            return DB::table($this->table)
                        ->where('id_navgation',$id)
                        ->update([
                            'status'    =>'0',
                            'delete_by' => $user_id,
                            'delete_at' => date('Y-m-d H:i:s')
                            ]);
        }else {
            return false;
        }

    }



}
