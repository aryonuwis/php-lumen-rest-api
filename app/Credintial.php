<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\DB;
use App\Controller as mod_control;

class Credintial extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //tabel name
    public $timestamps = false;
    public $table = "set_credintial";
    protected $fillable = [
        'id_credential', 'id_navgation',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id_credential', 'id_navgation'
    ];

    public function __construct()
    {
        $this->tabels = "set_credintial";
    }


    public function cek_navigation(){
        # code...
    }

    public function add_credintial($data=null){
        
        $data['create_by']  = $data['users'];
        $data['create_at']  = date('Y-m-d H:i:s');
        $data['status']     = '1';
        
        unset($data['users']);

        $return = DB::table('users_credintial')
                    ->insertGetId($data);
        
        return $return;
    }

    public function add_setcredintial($users_id, $id_credintial , $data=null ){
        
        $set_cred['id_credential']  = $id_credintial;
        $set_cred['id_navgation']   = $data->id_nav;
        $set_cred['create_by']      = $users_id;
        $set_cred['create_at']      = date('Y-m-d H:i:s');
        $set_cred['status']         = $data->status;

        $return = DB::table('set_credintial')
                    ->insert($set_cred);
                    
        return $return;
    }


    
}


?>