<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetCredintial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('set_credintial', function (Blueprint $table) {
            $table->bigIncrements('id', 30);
            $table->string('id_credential', 100)->nullable(false);
            $table->string('id_navgation', 100)->nullable(false);

            $table->string('create_by', 150)->nullable();
            $table->date('create_at')->nullable();
            $table->string('update_by', 150)->nullable();
            $table->date('update_at')->nullable();
            $table->string('delete_by', 150)->nullable();
            $table->date('delete_at')->nullable();
            $table->enum('status', ['0', '1', '2', '3'])->nullable()->default('1'); // 0 => delete ; 1 => active ; 2 => suspend ; 3 => block 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('set_credintial');
    }
}
