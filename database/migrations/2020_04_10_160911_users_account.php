<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_account', function (Blueprint $table) {
            
            $table->bigIncrements('id',30);
            $table->string('first_name',255)->nullable(false);
            $table->string('last_name',255)->nullable(true);
            $table->string('usersname', 255)->nullable(false)->unique(true);
            $table->string('photo_profile', 100)->nullable();
            $table->string('email', 255)->nullable(false)->unique();
            $table->enum('email_status', ['verified', 'unverified'])->nullable(false)->default('unverified');
            $table->string('phone', 100)->nullable();
            $table->enum('phone_status', ['verified', 'unverified'])->nullable()->default('unverified');
            $table->longText('password')->nullable(false)->default('text');
            $table->string('id_credential', 100)->nullable(false);

            $table->string('create_by', 150)->nullable();
            $table->date('create_at')->nullable();
            $table->string('update_by', 150)->nullable();
            $table->date('update_at')->nullable();
            $table->string('delete_by', 150)->nullable();
            $table->date('delete_at')->nullable();
            $table->enum('status', ['0', '1', '2', '3'])->nullable(false)->default('1'); // 0 => delete ; 1 => active ; 2 => suspend ; 3 => block 

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_account');
    }
}
