<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class users_set_credintial extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('set_credintial')->insert([
            'id_credential' => '1',
            'id_navgation'  => '1',
            'create_by'     => 'admin',
            'create_at'   => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);

        DB::table('set_credintial')->insert([
            'id_credential' => '1',
            'id_navgation'  => '2',
            'create_by'     => 'admin',
            'create_at'   => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);
        DB::table('set_credintial')->insert([
            'id_credential' => '1',
            'id_navgation'  => '3',
            'create_by'     => 'admin',
            'create_at'   => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);
        DB::table('set_credintial')->insert([
            'id_credential' => '1',
            'id_navgation'  => '4',
            'create_by'     => 'admin',
            'create_at'   => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);
        DB::table('set_credintial')->insert([
            'id_credential' => '1',
            'id_navgation'  => '5',
            'create_by'     => 'admin',
            'create_at'   => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);
        DB::table('set_credintial')->insert([
            'id_credential' => '1',
            'id_navgation'  => '6',
            'create_by'     => 'admin',
            'create_at'   => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);
        DB::table('set_credintial')->insert([
            'id_credential' => '1',
            'id_navgation'  => '7',
            'create_by'     => 'admin',
            'create_at'   => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);
    }
}
