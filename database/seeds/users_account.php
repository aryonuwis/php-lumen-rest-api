<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use illuminate\support\Str;

class users_account extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_account')->insert([
            'first_name'    => 'admin',
            'last_name'     => 'support',
            'usersname'     => 'nuwiadminsupport',
            'photo_profile' => '',
            'email'         => 'res.aryo1291@gmail.com',
            'email_status'  => 'verified',
            'phone'         => '',
            'phone_status'  => 'unverified',
            'password'      =>  Crypt::encrypt(Hash::make(md5('adminsupport69'))),
            'id_credential' => '1',
            'create_by'     => 'admin', //bawaaan pas pertama kali
            'create_at'     => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);
    }
}
