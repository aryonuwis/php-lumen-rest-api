<?php

use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;

class navigation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('navigation')->insert([
            'id_navgation'  => '1',
            'name'          => 'Credintial',
            'icon'          => 'fa fa-bars',
            'slug'          => '',
            'order_list'    => '1',
            'nav_parent_id' => '0',
            'create_by'     => 'admin',
            'create_at'     => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);

        DB::table('navigation')->insert([
            'id_navgation'  => '2',
            'name'          => 'Navigation',
            'icon'          => '',
            'slug'          => '',
            'order_list'    => '1',
            'nav_parent_id' => '1',
            'create_by'     => 'admin',
            'create_at'     => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);

        DB::table('navigation')->insert([
            'id_navgation'  => '3',
            'name'          => 'Navigation Level',
            'icon'          => '',
            'slug'          => '',
            'order_list'    => '2',
            'nav_parent_id' => '1',
            'create_by'     => 'admin',
            'create_at'     => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);

        DB::table('navigation')->insert([
            'id_navgation'  => '4',
            'name'          => 'Users',
            'icon'          => 'fa fa-users',
            'slug'          => '',
            'order_list'    => '2',
            'nav_parent_id' => '0',
            'create_by'     => 'admin',
            'create_at'     => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);

        DB::table('navigation')->insert([
            'id_navgation'  => '5',
            'name'          => 'My Profile',
            'icon'          => '',
            'slug'          => '',
            'order_list'    => '1',
            'nav_parent_id' => '4',
            'create_by'     => 'admin',
            'create_at'     => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);

        DB::table('navigation')->insert([
            'id_navgation'  => '6',
            'name'          => 'List',
            'icon'          => '',
            'slug'          => '',
            'order_list'    => '2',
            'nav_parent_id' => '4',
            'create_by'     => 'admin',
            'create_at'     => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);

        DB::table('navigation')->insert([
            'id_navgation'  => '7',
            'name'          => 'Register',
            'icon'          => '',
            'slug'          => '',
            'order_list'    => '3',
            'nav_parent_id' => '4',
            'create_by'     => 'admin',
            'create_at'     => date('Y-m-d h:i:s'),
            'status'        => '1'
        ]);
    }
}
