<?php

use Illuminate\Database\Seeder;
use illuminate\support\Facades\DB;

class users_credintial extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_credintial')->insert([
            'id_credential' => '1',
            'name'          => 'super_admin',
            'description'   => 'Ini credintial untuk dapat mengakses seluruh menu di dashboard dan public',
            'create_by'     => 'admin', //bawaaan pas pertama kali
            'create_at'     =>  date('Y-m-d h:i:s'),
            'status'        =>  '1'
        ]);
    }
}
